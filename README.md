# Exporting New Packages #
Use Export Package to create your own Custom Package.

1. Open the project you want to export assets from.
2. Choose Assets > Export Package� from the menu to bring up the Exporting Package dialog box. (See Fig 1: Exporting Package dialog box.)
3. In the dialog box, select the assets you want to include in the package by clicking on the boxes so they are checked.
	* Exclude the Unit Tests and the Rho.asmdef, those will screw up the assemblies of the project that imports the package.
4. Leave the include dependencies box checked to auto-select any assets used by the ones you have selected.
5. Click on Export to bring up File Explorer (Windows) or Finder (Mac) and choose where you want to store your package file. Name and save the package anywhere you like.

![Alt text](https://i.imgur.com/mYa6hsw.png, "Export" )

# Import Package #
You can import Standard Asset Packages, which are asset collections pre-made and supplied with Unity, and Custom Packages, which are made by people using Unity.

Choose Assets > Import Package > to import both types of package.

![Alt text](https://docs.unity3d.com/uploads/Main/CustomPackageInstallDialog.png "Import")