using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using rho;

namespace Tests
{
    public class ExternVarTests
    {
        [Test]
        public void IntVarSetsValue()
        {
            var intValue = ScriptableObject.CreateInstance<IntVariable>();

            intValue.Value = 5;
            Assert.AreEqual(5, intValue.Value);
        }

        [Test]
        public void FloatVarSetsValue()
        {
            var floatValue = ScriptableObject.CreateInstance<FloatVariable>();

            floatValue.Value = 5.1f;
            Assert.AreEqual(5.1f, floatValue.Value);
        }

        [Test]
        public void BoolVarSetsValue()
        {
            var booleanVariable = ScriptableObject.CreateInstance<BooleanVariable>();

            booleanVariable.Value = true;
            Assert.AreEqual(true, booleanVariable.Value);
        }

        [Test]
        public void IntVarEventCalled()
        {
            var intValue = ScriptableObject.CreateInstance<IntVariable>();
            
            intValue.Changed += (ExternalVariable<int> val, int oldValue, int newValue) => Assert.Pass();
            intValue.Value = 5;
        }

        [Test]
        public void IntVarEventArgsCorrect()
        {
            var intValue = ScriptableObject.CreateInstance<IntVariable>();
            
            intValue.Value = 0;
            intValue.Changed += (ExternalVariable<int> sender, int oldValue, int newValue) => {
                Assert.AreSame(intValue, sender);
                Assert.AreEqual(0, oldValue);
                Assert.AreEqual(5, newValue);
            };
            intValue.Value = 5;
        }
    }
}
