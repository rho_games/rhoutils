﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rho
{
    /// <summary>
    /// A Runtime set acts like a external collection that can be given to components who can then add/remove/observe for various uses.
    /// Example is a RuntimeSet<GameObject> that can be used to create a number of assets like: PlayerSet, BotPlayerSet, BlocksSet, etc.
    /// You then reference a set like this: [SerializeField] RuntimeGameObjectSet _activePlayersSet
    /// Source: https://youtu.be/raQ3iHhE_Kk?t=2382
    /// </summary>
    public abstract class RuntimeSet<T> : ScriptableObject, ICollection<T>
    {
        protected List<T> _items = new();

        #region Events
        public event System.Action SetChanged = delegate {};
        public event System.Action ItemAdded = delegate {};
        public event System.Action ItemRemoved = delegate {};
        #endregion

        #region ICollection<T>
        public virtual int Count => _items.Count;

        public virtual bool IsReadOnly => ((ICollection<T>)_items).IsReadOnly;

        public virtual void Add(T item)
        {
            _items.Add(item);
            SetChanged();
            ItemAdded();
        }

        public virtual void Clear()
        {
            _items.Clear();
            SetChanged();
            ItemRemoved();
        }

        public virtual bool Contains(T item) => _items.Contains(item);

        public virtual void CopyTo(T[] array, int arrayIndex)
        {
            _items.CopyTo(array, arrayIndex);
            SetChanged();
            ItemAdded();
        }

        public virtual IEnumerator<T> GetEnumerator() => _items.GetEnumerator();

        public virtual bool Remove(T item)
        {
            var rval =  _items.Remove(item);
            SetChanged();
            ItemRemoved();
            return rval;
        }

        IEnumerator IEnumerable.GetEnumerator() => _items.GetEnumerator();
        #endregion
    }
}
