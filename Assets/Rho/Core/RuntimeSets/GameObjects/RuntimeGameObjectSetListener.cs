using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace rho
{
    /// <summary>
    /// If the runtime set changes, invoke the onChange UnityEvent.
    /// </summary>
    public class RuntimeGameObjectSetListener : MonoBehaviour
    {
        [SerializeField] RuntimeGameObjectSet _runtimeSet = null;
        [SerializeField] UnityEvent _onChange;
        [SerializeField] UnityEvent _onItemAdded;
        [SerializeField] UnityEvent _onItemRemoved;

        void SetChanged()
        {
            _onChange.Invoke();
        }

        void ItemAdded()
        {
            _onItemAdded.Invoke();
        }

        void ItemRemoved()
        {
            _onItemRemoved.Invoke();
        }

        void OnEnable()
        {
            _runtimeSet.SetChanged += SetChanged;
            _runtimeSet.ItemAdded += ItemAdded;
            _runtimeSet.ItemRemoved += ItemRemoved;
        }

        void OnDisable()
        {
            _runtimeSet.SetChanged -= SetChanged;
            _runtimeSet.ItemAdded -= ItemAdded;
            _runtimeSet.ItemRemoved -= ItemRemoved;
        }
    }
}